import java.util.Random;
import java.io.*;
import java.util.*;
import java.io.IOException;

public class Player {
    List<Card> deck;
    boolean playing;

    public Player() {
        deck = new ArrayList<Card>();
        playing = true;
    };

    public Card drawCard() throws IOException {
        Card card = new Card();
        deck.add(card);
        return card;
    }

}
